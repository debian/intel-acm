History of 2nd_gen_i5_i7-SINIT
==============================

v51:

-  Security update for Dec 2011 Security Advisory ID: INTEL-SA-0003(www.intel.com/security)

-  No changes in v20 - v50, aligning version numbers across platforms

v19:
    -  Initial release
    -  Support for v2 of Launch Control Policy
    -  Supports OsSinitData v5
    -  Uses SinitMleData v8
    -  Uses Chipset ACM Information Table v4
    -  Enforces that LCP Data blobs must reside in DMA-protected memory
    -  Returns MLE page table base addr in ECX after SENTER
